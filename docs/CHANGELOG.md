# [1.1.0](https://gitlab.com/caiquereboucas/semantic-release-test/compare/v1.0.0...v1.1.0) (2020-09-25)


### Features

* test 11 ([e44b0fd](https://gitlab.com/caiquereboucas/semantic-release-test/commit/e44b0fde0e4c3e50e471e3d8f0ca1c3e2b3d4c58))

# 1.0.0 (2020-09-25)


### Bug Fixes

* branch release configuration ([fcd4f28](https://gitlab.com/caiquereboucas/semantic-release-test/commit/fcd4f28bc5216f62ccc7bfbd613c4a223217a5f7))
* ci file ([d43ba85](https://gitlab.com/caiquereboucas/semantic-release-test/commit/d43ba85fcdac60ee04fc9befb4c66d9124095984))
* configuration changelog ([ae23ce8](https://gitlab.com/caiquereboucas/semantic-release-test/commit/ae23ce8c703b767cfb01306e9d0643f2d816919f))
* create token with gitlab-rails runner ([d246cd1](https://gitlab.com/caiquereboucas/semantic-release-test/commit/d246cd1635b58e9bf6bfe3b45ca0a8202bfb94a2))
* field name in package.json ([8c242bd](https://gitlab.com/caiquereboucas/semantic-release-test/commit/8c242bd8ceda6cef678495f8cbd3a41b32de68c3))
* remove changelog file ([0226b2f](https://gitlab.com/caiquereboucas/semantic-release-test/commit/0226b2f6a10044e0f10233dc9266703b2e9893e5))
* remove git from plugins ([5ac51e8](https://gitlab.com/caiquereboucas/semantic-release-test/commit/5ac51e81b485d312b0ae587cb21336c2d817c0e9))
* remove repository field ([e56384c](https://gitlab.com/caiquereboucas/semantic-release-test/commit/e56384c308c6afc0346fdf2caa1495bc383c94e6))
* reset ci and release configuration ([0752414](https://gitlab.com/caiquereboucas/semantic-release-test/commit/07524144f2346d70c98e6b1cc685ebe96bf6ad60))
* reset ci file ([189fd3b](https://gitlab.com/caiquereboucas/semantic-release-test/commit/189fd3bc1b7cb495490aec450c2a461520970323))
* script job ([fd656a3](https://gitlab.com/caiquereboucas/semantic-release-test/commit/fd656a3b92c9cd97d5e8a8d06c68229c50d5faa5))
* set @semantic-release/git ([8f92485](https://gitlab.com/caiquereboucas/semantic-release-test/commit/8f924854fe9d5553330213011fa151e4f11d1e79))
* set configuration in plugins ([4dbb4e1](https://gitlab.com/caiquereboucas/semantic-release-test/commit/4dbb4e1b8aa9039885551b1ce269529d6e54344b))
* set token in package.json ([9db0107](https://gitlab.com/caiquereboucas/semantic-release-test/commit/9db0107aa1f92507cbf39bbc17e39a89a8368bc7))
* set user token in script ([69e61e0](https://gitlab.com/caiquereboucas/semantic-release-test/commit/69e61e023264602e95945cdd7b5230e2533c29a7))


### Features

* .releaserc.json file ([3a114c2](https://gitlab.com/caiquereboucas/semantic-release-test/commit/3a114c2f9f9e97e71a20682be6bc7fcf09fd3758))
* create package.json ([73d89a9](https://gitlab.com/caiquereboucas/semantic-release-test/commit/73d89a97b484f2383c18debb1d794e1091ef55fd))
* create readme file ([efe4b4f](https://gitlab.com/caiquereboucas/semantic-release-test/commit/efe4b4fdb112330a5feca2e9e385f0de22b047a1))
* delete changelog.md ([9acafe7](https://gitlab.com/caiquereboucas/semantic-release-test/commit/9acafe782554193f5ced4978530a1dd20e721ddc))
* set gl_token in ci file ([30848ca](https://gitlab.com/caiquereboucas/semantic-release-test/commit/30848ca6caaf545e2a8782ff44730a06d1ce257a))
* test 10 ([c7b545e](https://gitlab.com/caiquereboucas/semantic-release-test/commit/c7b545edfd9e34e69254468791c48a6cf4ada05e))
* test 4 ([2997de4](https://gitlab.com/caiquereboucas/semantic-release-test/commit/2997de44d133fd0e114bb4abf28161b87bd2cc7a))
* test 5 ([321e7b1](https://gitlab.com/caiquereboucas/semantic-release-test/commit/321e7b1df817ab3c5410e57d68ddcbe46d08fd76))
* test 6 ([a78c12d](https://gitlab.com/caiquereboucas/semantic-release-test/commit/a78c12dc15aa33950d2cfb9030b7bb78d6da755f))
* test 7 ([9f29a4d](https://gitlab.com/caiquereboucas/semantic-release-test/commit/9f29a4d83ec9fefa76de1dcd5dfd9ece87391285))
* test 8 ([c8d7538](https://gitlab.com/caiquereboucas/semantic-release-test/commit/c8d7538f206e5542831e5582ea6924d6281ac9b6))
* test 9 ([9944e2d](https://gitlab.com/caiquereboucas/semantic-release-test/commit/9944e2d72bf7a5f5ef20e515685c79a897267594))
* test3 ([a117762](https://gitlab.com/caiquereboucas/semantic-release-test/commit/a1177629b43595a81fc823cff66b755f7c3a83c0))
* token configuration ([2eb491c](https://gitlab.com/caiquereboucas/semantic-release-test/commit/2eb491c25fc53aea1bacccf1b8748045a954e701))
* update branch target of release ([76419b2](https://gitlab.com/caiquereboucas/semantic-release-test/commit/76419b20becfaaecd8c68d13f6cc758a186e4c9b))
